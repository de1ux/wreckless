PATH := $(GOPATH)/bin:$(PATH)

api:
	go get -u github.com/golang/protobuf/protoc-gen-go
	rm -rf app/src/generated generated
	mkdir -p app/src/generated generated
	protoc \
		--plugin="protoc-gen-ts=app/node_modules/.bin/protoc-gen-ts" \
		--js_out="import_style=commonjs,binary:app/src/generated" \
		--ts_out=service=true:"app/src/generated" \
		--go_out=plugins=grpc:"generated" \
		api/*

.PHONY: api