FROM alpine:3.6

COPY wreckless /usr/bin/wreckless

COPY app/dist /app/dist
COPY app/index.html /app/index.html

CMD ["wreckless"]