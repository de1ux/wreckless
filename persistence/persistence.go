package persistence

import (
	"cloud.google.com/go/datastore"
	model "gitlab.com/de1ux/wreckless/generated/api"
)

type Persistor interface {
	CreateOrUpdateJobDefinition(*model.JobDefinition) (*model.JobDefinition, error)

	CreateProject(*model.Project) (*model.Project, error)

	ReadProjects() ([]*model.Project, error)

	ReadProjectByID(string) (*model.Project, error)

	ReadJobsByProjectID(string) ([]*model.JobDefinition, error)

	ReadJobDefinitions() ([]*model.JobDefinition, error)

	ReadJobDefinitionById(string) (*model.JobDefinition, error)

	ReadJobOptionById(string) (*model.JobDefinitionOption, error)

	DeleteJobDefinition(*model.JobDefinition) error

	CreateExecution(*model.Execution) (*model.Execution, error)

	GetExecutionFromExecutionId(string) (*model.Execution, error)

	GetExecutionFromExecutionHash(string) (*model.Execution, error)

	UpdateExecution(*model.Execution) error

	GetMetrics() ([]*model.Metrics, error)

	CreateOrUpdateMetrics(*model.Metrics) (*model.Metrics, error)

	GetProjectKeys() ([]*datastore.Key, error)

	GetExecutionsFromProjectKey(string) ([]*model.Execution, error)
}
