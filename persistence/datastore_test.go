package persistence

import (
	"fmt"
	"testing"

	model "gitlab.com/de1ux/wreckless/generated/api"

	"cloud.google.com/go/datastore"
	"golang.org/x/net/context"
)

func TestPassesWithNoErrors(t *testing.T) {
	internalGetKeysByAncestor = func(p *v1, ctx context.Context, key *datastore.Key) ([]*datastore.Key, error) {
		key, err := datastore.DecodeKey("EhgKDUpvYkRlZmluaXRpb24QgICAgLzLigo")
		if err != nil {
			return nil, fmt.Errorf("Failed to Decode Project Key: %s", err)
		}
		return []*datastore.Key{
			key, key, key, key, key, key, key, key, key, key,
		}, nil
	}
	internalReadJobDefinitionByID = func(p *v1, key *datastore.Key) (*model.JobDefinition, error) {
		return &model.JobDefinition{}, nil
	}

	p := &v1{nil, nil}
	jobs, err := p.ReadJobsByProjectID("EhgKDUpvYkRlZmluaXRpb24QgICAgLzLigo")
	if err != nil {
		t.Fatalf("Unexpected Error: %s", err)
	}
	if len(jobs) != 10 {
		t.Fatalf("Expected 10. Got %d", len(jobs))
	}
}

func TestNoJobsForAProject(t *testing.T) {
	internalGetKeysByAncestor = func(p *v1, ctx context.Context, key *datastore.Key) ([]*datastore.Key, error) {
		return []*datastore.Key{}, nil
	}

	p := &v1{nil, nil}
	jobs, err := p.ReadJobsByProjectID("EhgKDUpvYkRlZmluaXRpb24QgICAgLzLigo")
	if err != nil {
		t.Fatalf("Unexpected Error: %s", err)
	}
	if len(jobs) != 0 {
		t.Fatalf("Expected 0. Got %d", len(jobs))
	}
}

func TestPassesButWithSomeFailedJobs(t *testing.T) {
	internalGetKeysByAncestor = func(p *v1, ctx context.Context, key *datastore.Key) ([]*datastore.Key, error) {
		key, err := datastore.DecodeKey("EhgKDUpvYkRlZmluaXRpb24QgICAgLzLigo")
		if err != nil {
			return nil, fmt.Errorf("Failed to Decode Project Key: %s", err)
		}
		return []*datastore.Key{
			key, key, key, key, key, key, key, key, key, key,
		}, nil
	}
	internalReadJobDefinitionByID = func() func(p *v1, key *datastore.Key) (*model.JobDefinition, error) {
		n := -1

		return func(p *v1, key *datastore.Key) (*model.JobDefinition, error) {
			n += 1
			if n > 6 {
				return nil, fmt.Errorf("Error")
			}
			return &model.JobDefinition{}, nil

		}
	}()

	p := &v1{nil, nil}
	jobs, err := p.ReadJobsByProjectID("EhgKDUpvYkRlZmluaXRpb24QgICAgLzLigo")
	if err != nil {
		t.Fatalf("Unexpected Error: %s", err)
	}
	if len(jobs) != 7 {
		t.Fatalf("Expected 7. Got %d", len(jobs))
	}
}
