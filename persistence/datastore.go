package persistence

import (
	"fmt"
	"io/ioutil"
	"os"

	model "gitlab.com/de1ux/wreckless/generated/api"

	"cloud.google.com/go/datastore"
	vaultAPI "github.com/hashicorp/vault/api"
	"golang.org/x/net/context"
)

var (
	wrecklessVaultPath = ""
)

func NewDatastorePersistor() (Persistor, error) {
	ctx := context.Background()

	if os.Getenv("DEBUG") == "" {
		wd, err := os.Getwd()
		if err != nil {
			return nil, err
		}
		credentialsPath := fmt.Sprintf("%s/credentials.json", wd)

		vaultClient, err := vaultAPI.NewClient(vaultAPI.DefaultConfig())

		if err != nil {
			return nil, err
		}

		secret, err := vaultClient.Logical().Read(wrecklessVaultPath)
		if err != nil {
			return nil, err
		}

		credentialsString, ok := secret.Data["gcp_credentials_json"].(string)
		if !ok {
			return nil, fmt.Errorf("Failed to get GCP credentials for vault")
		}
		credentialsBytes := []byte(credentialsString)

		if err := ioutil.WriteFile(credentialsPath, credentialsBytes, 0600); err != nil {
			return nil, err
		}

		if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", credentialsPath); err != nil {
			return nil, err
		}
		println("Set credentials path to " + credentialsPath)
	}

	client, err := datastore.NewClient(ctx, os.Getenv("DATASTORE_PROJECT_ID"))
	if err != nil {
		return nil, err
	}

	return &v1{client, ctx}, nil
}

type v1 struct {
	client *datastore.Client
	ctx    context.Context
}

// TODO - this is super jank, use a property save loader to automagically hydrate options
type jobDefinitionWrapper struct {
	Image string
	Name  string
}

type projectWrapper struct {
	Name string
}

type jobDefinitionOptionWrapper struct {
	Name        string
	Required    bool
	Default     string
	Description string
	Allowed     []string
}

type executionWrapper struct {
	JobDefinitionKey *datastore.Key
	Hash             string
	Status           string
	OnSuccess        string
	OnFailure        string
	OnStart          string
	ScheduleType     string
	ScheduleValue    string
	Started          int64
	Ended            int64
}

type executionOptionWrapper struct {
	JobDefinitionOptionKey *datastore.Key
	Value                  string
}

type metricWrapper struct {
	ProjectKey        *datastore.Key
	NumRunning        int64
	NumSucceeded      int64
	NumFailed         int64
	NumCreated        int64
	NumErrored        int64
	MedianRunningTime float64
	MeanRunningTime   float64
	PercentileTime    float64
}

var (
	internalGetKeysByAncestor     = getKeysByAncestor
	internalReadJobDefinitionByID = readJobDefinitionByID
)

// TODO - satisfy the update
func (p *v1) CreateOrUpdateJobDefinition(job *model.JobDefinition) (*model.JobDefinition, error) {
	if job.Key == "" {
		return p.createJobDefinition(job)
	}
	return p.updateJobDefinition(job)
}

func (p *v1) CreateProject(project *model.Project) (*model.Project, error) {
	pKey, err := p.client.Put(
		p.ctx,
		datastore.NameKey("Project", "", nil),
		&projectWrapper{
			project.Name,
		})
	if err != nil {
		return project, fmt.Errorf("Failed to create Project: %s", err)
	}
	project.Key = pKey.Encode()
	return project, nil
}

func (p *v1) ReadProjects() ([]*model.Project, error) {
	pKey, err := p.client.GetAll(p.ctx, datastore.NewQuery("Project").KeysOnly(), nil)
	if err != nil {
		return nil, fmt.Errorf("Failed to get Project keys: %s", err)
	}

	projects := make([]*model.Project, len(pKey))

	for i, pKey := range pKey {
		proj, err := p.ReadProjectByID(pKey.Encode())
		if err != nil {
			return nil, fmt.Errorf("Failed to read Project: %s", err)
		}
		projects[i] = proj
	}

	return projects, nil
}

func (p *v1) ReadProjectByID(key string) (*model.Project, error) {
	wrapper := &projectWrapper{}

	pKey, err := datastore.DecodeKey(key)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode key %s %s", key, err)
	}

	if err := p.client.Get(p.ctx, pKey, wrapper); err != nil {
		return nil, fmt.Errorf("Failed to hydrate project wrapper: %s", err)
	}

	return &model.Project{
		Name: wrapper.Name,
		Key:  pKey.Encode(),
	}, nil
}

func (p *v1) createJobDefinition(job *model.JobDefinition) (*model.JobDefinition, error) {
	// Save the job definition wrapper
	pKey, err := datastore.DecodeKey(job.ProjectKey)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode key %s %s", pKey, err)
	}
	dKey, err := p.client.Put(
		p.ctx,
		datastore.NameKey("JobDefinition", "", pKey),
		&jobDefinitionWrapper{
			job.Image, job.Name,
		})

	if err != nil {
		return job, fmt.Errorf("Failed to create JobDefinition: %s", err)
	}

	// Save the job options with the job definition wrapper as the parent key
	for _, o := range job.Options {
		oKey := datastore.NameKey("JobDefinitionOption", "", dKey)
		if _, err := p.client.Put(
			p.ctx,
			oKey,
			&jobDefinitionOptionWrapper{
				o.Name, o.Required, o.Default, o.Description, o.Allowed,
			}); err != nil {
			return job, fmt.Errorf("Failed to put JobDefinitionOption: %s", err)
		}
	}
	job.Key = dKey.Encode()

	return job, err
}

func (p *v1) updateJobDefinition(job *model.JobDefinition) (*model.JobDefinition, error) {
	dKey, err := datastore.DecodeKey(job.Key)
	if err != nil {
		return nil, err
	}

	if _, err = p.client.Put(
		p.ctx,
		dKey,
		&jobDefinitionWrapper{
			job.Image, job.Name,
		}); err != nil {
		return job, fmt.Errorf("Failed to update JobDefinition: %s", err)
	}

	for _, o := range job.Options {
		oKey, err := datastore.DecodeKey(o.Key)
		if err != nil {
			return nil, err
		}

		if _, err := p.client.Put(
			p.ctx,
			oKey,
			&jobDefinitionOptionWrapper{
				o.Name, o.Required, o.Default, o.Description, o.Allowed,
			}); err != nil {
			return job, fmt.Errorf("Failed to put JobDefinitionOption: %s", err)
		}
	}

	return job, err
}

func (p *v1) ReadJobDefinitions() ([]*model.JobDefinition, error) {
	dKeys, err := p.client.GetAll(p.ctx, datastore.NewQuery("JobDefinition").KeysOnly(), nil)
	if err != nil {
		return nil, fmt.Errorf("Failed to get JobDefinitions: %s", err)
	}

	definitions := make([]*model.JobDefinition, len(dKeys))

	for i, dKey := range dKeys {
		d, err := p.ReadJobDefinitionById(dKey.Encode())
		if err != nil {
			return nil, fmt.Errorf("Failed to read job definition: %s", err)
		}
		definitions[i] = d
	}

	return definitions, nil
}

func (p *v1) DeleteJobDefinition(job *model.JobDefinition) error {
	key, err := datastore.DecodeKey(job.Key)
	if err != nil {
		return fmt.Errorf("Failed to decode job definition key: %s", err)
	}

	keys := []*datastore.Key{key}
	for _, o := range job.Options {
		oKey, err := datastore.DecodeKey(o.Key)
		if err != nil {
			return fmt.Errorf("Failed to decode job definition option key: %s", err)
		}
		keys = append(keys, oKey)
	}

	return p.client.DeleteMulti(p.ctx, keys)
}

func getKeysByAncestor(p *v1, ctx context.Context, key *datastore.Key) ([]*datastore.Key, error) {
	return p.client.GetAll(ctx, datastore.NewQuery("JobDefinition").Ancestor(key).KeysOnly(), nil)
}

func (p *v1) ReadJobsByProjectID(key string) ([]*model.JobDefinition, error) {

	pKey, err := datastore.DecodeKey(key)
	if err != nil {
		return nil, fmt.Errorf("Failed to Decode Project Key: %s", err)
	}

	dKeys, err := internalGetKeysByAncestor(p, p.ctx, pKey)
	if err != nil {
		return nil, fmt.Errorf("Failed to find Job Definition Keys from Project Key: %s", err)
	}

	jobs := make([]*model.JobDefinition, len(dKeys))

	channel := make(chan *model.JobDefinition, len(dKeys))
	defer close(channel)

	for _, dKey := range dKeys {
		go func(key *datastore.Key) {
			definition, err := internalReadJobDefinitionByID(p, key)
			if err != nil {
				fmt.Printf("Failed to find Job Definition: %s\n", err)
			}
			channel <- definition
		}(dKey)
	}
	for i := 0; i < len(dKeys); i++ {
		jobs[i] = <-channel
	}
	filtered := []*model.JobDefinition{}
	for _, job := range jobs {
		if job != nil {
			filtered = append(filtered, job)
		}
	}
	return filtered, nil
}

func readJobDefinitionByID(p *v1, key *datastore.Key) (*model.JobDefinition, error) {
	return p.ReadJobDefinitionById(key.Encode())
}

func (p *v1) ReadJobDefinitionById(key string) (*model.JobDefinition, error) {

	wrapper := &jobDefinitionWrapper{}

	dKey, err := datastore.DecodeKey(key)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode key %s %s", key, err)
	}

	if err := p.client.Get(p.ctx, dKey, wrapper); err != nil {
		return nil, fmt.Errorf("Failed to hydrate job definition wrapper: %s", err)
	}

	options := make([]*jobDefinitionOptionWrapper, 0)
	oKeys, err := p.client.GetAll(p.ctx, datastore.NewQuery("JobDefinitionOption").Ancestor(dKey), &options)
	if err != nil {
		return nil, fmt.Errorf("Failed to get JobDefinitionOptions: %s", err)
	}

	optionModels := make([]*model.JobDefinitionOption, len(oKeys))
	for i, option := range options {
		optionModels[i] = &model.JobDefinitionOption{
			Key:         oKeys[i].Encode(),
			Name:        option.Name,
			Required:    option.Required,
			Default:     option.Default,
			Description: option.Description,
			Allowed:     option.Allowed,
		}
	}

	return &model.JobDefinition{
		Image:   wrapper.Image,
		Name:    wrapper.Name,
		Options: optionModels,
		Key:     dKey.Encode(),
	}, nil
}

func (p *v1) CreateExecution(execution *model.Execution) (*model.Execution, error) {
	dKey, err := datastore.DecodeKey(execution.JobDefinitionKey)
	if err != nil {
		return nil, err
	}

	eKey, err := p.client.Put(
		p.ctx,
		datastore.NameKey("Execution", "", dKey),
		&executionWrapper{
			dKey, execution.Hash, execution.Status.String(), execution.OnSuccess, execution.OnFailure, execution.OnStart, execution.ScheduleType.String(), execution.ScheduleValue, execution.Started, execution.Ended,
		})

	if err != nil {
		return nil, fmt.Errorf("Failed to create Execution: %s", err)
	}

	execution.Key = eKey.Encode()
	return execution, nil
}

func (p *v1) UpdateExecution(execution *model.Execution) error {
	dKey, err := datastore.DecodeKey(execution.JobDefinitionKey)
	if err != nil {
		return fmt.Errorf("UpdateExecution: 1 %s %s", dKey, err)
	}

	eKey, err := datastore.DecodeKey(execution.Key)
	if err != nil {
		return fmt.Errorf("UpdateExecution: 2 %s %s", eKey, err)
	}

	if _, err := p.client.Put(
		p.ctx,
		eKey,
		&executionWrapper{
			dKey, execution.Hash, execution.Status.String(), execution.OnSuccess, execution.OnFailure, execution.OnStart, execution.ScheduleType.String(), execution.ScheduleValue, execution.Started, execution.Ended,
		}); err != nil {
		return fmt.Errorf("Failed on Put: %s", err)
	}

	for _, option := range execution.Options {
		oKey, err := datastore.DecodeKey(option.JobDefinitionOptionKey)
		if err != nil {
			return fmt.Errorf("Here! %s", err)
		}

		if _, err := p.client.Put(
			p.ctx,
			datastore.NameKey("ExecutionOption", "", eKey),
			&executionOptionWrapper{
				oKey,
				option.Value,
			}); err != nil {
			return fmt.Errorf("Failed on Option Put: %s", err)
		}
	}

	return nil
}

func (p *v1) ReadJobOptionById(key string) (*model.JobDefinitionOption, error) {
	oKey, err := datastore.DecodeKey(key)
	if err != nil {
		return nil, err
	}
	option := &model.JobDefinitionOption{}
	return option, p.client.Get(p.ctx, oKey, option)
}

func (p *v1) GetExecutionFromExecutionId(key string) (*model.Execution, error) {
	eKey, err := datastore.DecodeKey(key)
	if err != nil {
		return nil, fmt.Errorf("GetExecutionFromExecutionId: %s", err)
	}

	execution := &executionWrapper{}
	err = p.client.Get(p.ctx, eKey, execution)
	if err != nil {
		return nil, fmt.Errorf("GetExecutionFromExecutionId: %s", err)
	}

	options := make([]*executionOptionWrapper, 0)
	oKeys, err := p.client.GetAll(p.ctx, datastore.NewQuery("ExecutionOption").Ancestor(eKey), &options)
	if err != nil {
		return nil, fmt.Errorf("GetExecutionFromExecutionId: %s", err)
	}

	optionModels := make([]*model.ExecutionOption, len(oKeys))
	for i, option := range options {
		optionModels[i] = &model.ExecutionOption{
			Key:   oKeys[i].Encode(),
			Value: option.Value,
		}
	}

	// This made me sad to type
	var scheduleType model.Execution_ScheduleType
	if execution.ScheduleType == "NOW" {
		scheduleType = model.Execution_NOW
	} else if execution.ScheduleType == "LATER" {
		scheduleType = model.Execution_LATER
	}

	// Ok this is getting rediculous
	var status model.Execution_Status
	if execution.Status == "CREATED" {
		status = model.Execution_CREATED
	} else if execution.Status == "RUNNING" {
		status = model.Execution_RUNNING
	} else if execution.Status == "SUCCEEDED" {
		status = model.Execution_SUCCEEDED
	} else if execution.Status == "FAILED" {
		status = model.Execution_FAILED
	} else if execution.Status == "ERRORED" {
		status = model.Execution_ERRORED
	}

	return &model.Execution{
		JobDefinitionKey: execution.JobDefinitionKey.Encode(),
		Hash:             execution.Hash,
		OnSuccess:        execution.OnSuccess,
		OnFailure:        execution.OnFailure,
		OnStart:          execution.OnStart,
		ScheduleValue:    execution.ScheduleValue,
		ScheduleType:     scheduleType,
		Status:           status,
		Options:          optionModels,
		Key:              eKey.Encode(),
	}, nil
}

func (p *v1) GetExecutionFromExecutionHash(hash string) (*model.Execution, error) {
	keys, err := p.client.GetAll(p.ctx, datastore.NewQuery("Execution").Filter("Hash =", hash).Limit(1).KeysOnly(), nil)
	if err != nil {
		return nil, fmt.Errorf("Failed to get all: %s", err)
	}

	if len(keys) != 1 {
		return nil, fmt.Errorf("Failed to find execution with hash %s", hash)
	}

	encoded := keys[0].Encode()
	println(encoded)
	return p.GetExecutionFromExecutionId(encoded)
}

func (p *v1) GetMetricsFromMetricID(key string) (*model.Metrics, error) {
	mKey, err := datastore.DecodeKey(key)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode Metric Key: %s", err)
	}
	metric := &metricWrapper{}
	err = p.client.Get(p.ctx, mKey, metric)
	if err != nil {
		return nil, fmt.Errorf("Failed to get metric: %s", err)
	}
	return &model.Metrics{
		ProjectKey:        metric.ProjectKey.Encode(),
		NumCreated:        metric.NumCreated,
		NumErrored:        metric.NumErrored,
		NumFailed:         metric.NumFailed,
		NumRunning:        metric.NumRunning,
		NumSucceeded:      metric.NumSucceeded,
		MeanRunningTime:   metric.MeanRunningTime,
		MedianRunningTime: metric.MedianRunningTime,
		PercentileTime:    metric.PercentileTime,
		Key:               mKey.Encode(),
	}, nil
}

func (p *v1) GetMetrics() ([]*model.Metrics, error) {
	mKeys, err := p.client.GetAll(p.ctx, datastore.NewQuery("Metric").KeysOnly(), nil)
	if err != nil {
		return nil, fmt.Errorf("Failed to get Metric Keys: %s", err)
	}
	metrics := make([]*model.Metrics, len(mKeys))
	for i, mKey := range mKeys {
		metrics[i], err = p.GetMetricsFromMetricID(mKey.Encode())
		if err != nil {
			return nil, fmt.Errorf("Failed to get Metric: %s", err)
		}
	}
	return metrics, nil
}

func (p *v1) CreateOrUpdateMetrics(metric *model.Metrics) (*model.Metrics, error) {

	pKey, err := datastore.DecodeKey(metric.ProjectKey)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode key")
	}
	proj, err := p.client.GetAll(p.ctx, datastore.NewQuery("Metric").Ancestor(pKey).KeysOnly(), nil)
	if len(proj) <= 0 {
		return p.createMetrics(metric)
	}
	return p.updateMetrics(metric)
}

func (p *v1) createMetrics(metric *model.Metrics) (*model.Metrics, error) {
	pKey, err := datastore.DecodeKey(metric.ProjectKey)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode Project Key while creating metrics: %s", err)
	}
	mKey, err := p.client.Put(
		p.ctx,
		datastore.NameKey("Metric", "", pKey),
		&metricWrapper{
			pKey, metric.NumRunning, metric.NumSucceeded, metric.NumFailed, metric.NumCreated, metric.NumErrored, metric.MedianRunningTime, metric.MeanRunningTime, metric.PercentileTime,
		})
	if err != nil {
		return nil, fmt.Errorf("Failed to create Metric: %s", err)
	}
	metric.Key = mKey.Encode()
	_, err = p.client.GetAll(p.ctx, datastore.NewQuery("Metric").Ancestor(pKey).KeysOnly(), nil)

	return metric, err
}

// TODO: Creating works properly, something with reading metric.Key in the updateMetric function doesn't work.
func (p *v1) updateMetrics(metric *model.Metrics) (*model.Metrics, error) {
	pKey, err := datastore.DecodeKey(metric.ProjectKey)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode Project Key while updating metrics: %s", err)
	}
	mKey, err := datastore.DecodeKey(metric.Key)

	if err != nil {
		return nil, fmt.Errorf("Failed to decode Metric Key: %s", err)
	}
	if _, err := p.client.Put(
		p.ctx,
		mKey,
		&metricWrapper{
			pKey, metric.NumRunning, metric.NumSucceeded, metric.NumFailed, metric.NumCreated, metric.NumErrored, metric.MedianRunningTime, metric.MeanRunningTime, metric.PercentileTime,
		}); err != nil {
		return metric, fmt.Errorf("Failed to put Metric: %s", err)
	}

	return metric, nil
}

func (p *v1) GetProjectKeys() ([]*datastore.Key, error) {
	pKeys, err := p.client.GetAll(p.ctx, datastore.NewQuery("Project").KeysOnly(), nil)
	if err != nil {
		return nil, fmt.Errorf("Failed to get Project Keys: %s", err)
	}
	return pKeys, nil
}

func (p *v1) GetExecutionsFromProjectKey(key string) ([]*model.Execution, error) {
	pKey, err := datastore.DecodeKey(key)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode Project Key: %s", err)
	}
	eKeys, err := p.client.GetAll(p.ctx, datastore.NewQuery("Execution").Ancestor(pKey).KeysOnly(), nil)
	if err != nil {
		return nil, fmt.Errorf("Failed to get Execution Keys: %s", err)
	}
	executions := make([]*model.Execution, len(eKeys))
	for i, eKey := range eKeys {
		executions[i], err = p.GetExecutionFromExecutionId(eKey.Encode())
		if err != nil {
			return nil, fmt.Errorf("Failed to get Execution: %s", err)
		}
	}

	return executions, nil
}
