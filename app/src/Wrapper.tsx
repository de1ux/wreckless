import {AppBar, IconButton, Toolbar, Typography} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import * as React from 'react';
import {Store} from 'redux';

import {Debugger} from './components/Debugger';
import {GlobalMessenger} from './components/GlobalMessenger';
import {State} from './components/Store';
import {Sidebar} from './Sidebar';

const rootStyle = {
    padding: 5,
    margin: 5
};

interface WrapperProps {
    component: JSX.Element;
    store: Store<State>;
}

interface WrapperState {
    open: boolean
}

export class Wrapper extends React.Component<WrapperProps, WrapperState> {

    public readonly state: WrapperState = {
        open: false,
    };

    openMenu = (open: boolean) => () =>
        this.setState({open: open});


    render() {
        return (
            <div style={{flexGrow: 1}}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton color="inherit" aria-label="Menu" onClick={this.openMenu(true)}>
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="title" color="inherit" style={{flex: 1}}>
                            Wreckless
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div style={rootStyle}>
                    <GlobalMessenger store={this.props.store}/>
                    {this.props.component}
                    <br/>
                    <Debugger store={this.props.store}/>
                </div>
                <Sidebar open={this.state.open} onClose={this.openMenu(false)}/>
            </div>
        );
    }
}