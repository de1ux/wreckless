export function getProjectID(): string {
    let projectId = window.location.href.split("/");
    return projectId[projectId.length - 1];
}