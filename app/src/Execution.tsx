import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import * as React from 'react';
import { Store } from 'redux';

import { Debugger } from './components/Debugger';
import { GlobalMessenger } from './components/GlobalMessenger';
import { CreateGlobalStore, State } from './components/Store';

let store: Store<State> = CreateGlobalStore();

const rootStyle = {
    padding: 5,
    margin: 5
};

export const Execution: React.StatelessComponent<{}> = () => {
    return (
        <div>
            <AppBar position="sticky">
                <Toolbar>
                    <IconButton color="inherit" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="headline" color="inherit">
                        Wreckless
                    </Typography>
                </Toolbar>
            </AppBar>
            <div style={rootStyle}>
                <GlobalMessenger store={store} />
                Text!
                <br />
                <Debugger store={store} />
            </div>
        </div>
    );
};
