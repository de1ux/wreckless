import {Grid} from '@material-ui/core';
import * as React from 'react';
import * as ReactChart from 'react-chartjs-2';
import {Store} from 'redux';
import {grpc} from '../node_modules/grpc-web-client';

import {CreateGlobalStore, State} from './components/Store';
import {Metrics} from './generated/api/metrics_pb';
import {Empty} from './generated/api/service_pb';
import {WrecklessService} from './generated/api/service_pb_service';
import {Wrapper} from './Wrapper';

let store: Store<State> = CreateGlobalStore();
let width: number;
let height: number;
let barGraphMetrics: Array<number> = [];
let lineGraphMetrics: Array<number> = [];

interface DashboardState {
    metrics: Array<Metrics>;
    updated: boolean;
    newMetrics: Array<Metrics>;
}

const LineGraphData = {
    labels: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    datasets: [
        {
            fill: false,
            label: 'Average Execution Time',
            borderColor: 'rgba(255,99,132,1)', // 'rgba(153,51,255,1)'
            borderWidth: 2,
            pointHitRadius: 12,
            data: lineGraphMetrics // [10, 5, 11, 12, 11, 8, 15]
        }
    ]
};

const BarGraphData = {
    labels: ['Succeeded', 'Failed', 'Created', 'Errored'],
    datasets: [
        {
            label: 'Execution Status',
            backgroundColor: ['rgba(0,179,0,0.5)', 'rgba(230,0,0,0.5)', 'rgba(0,0,204,0.5)', 'rgba(220,0,0,0.5)'],
            data: barGraphMetrics
        }
    ]
};

export class Dashboard extends React.Component<DashboardState, {}> {

    public readonly state: DashboardState = {
        metrics: Array<Metrics>(),
        updated: false,
        newMetrics: Array<Metrics>(),
    };

    componentWillMount() {
        this.getGraphMetrics();
    }

    getGraphMetrics() {
        width = window.screen.width / 2;
        height = window.screen.height / 3;
        grpc.invoke(WrecklessService.GetMetrics, {
            request: new Empty(),
            host: window.location.origin,
            onMessage: ((met: Metrics) => {
                this.setState({
                    newMetrics: this.state.newMetrics.concat(met),
                });
            }),
            onEnd: (res: any) => {
                this.setState({metrics: this.state.newMetrics});
                this.countStatus();
                this.countTime();
            }
        });
    }

    countStatus() {
        let succeeded = 0;
        let failed = 0;
        let created = 0;
        let errored = 0;
        for (var i = 0; i < this.state.metrics.length; i++) {
            succeeded += this.state.metrics[i].getNumsucceeded();
            failed += this.state.metrics[i].getNumfailed();
            created += this.state.metrics[i].getNumcreated();
            errored += this.state.metrics[i].getNumerrored();
        }
        barGraphMetrics[0] = succeeded;
        barGraphMetrics[1] = failed;
        barGraphMetrics[2] = created;
        barGraphMetrics[3] = errored;
        this.addData();
    }

    countTime() {
        let meanTime = 0;
        for (var i = 0; i < this.state.metrics.length; i++) {
            meanTime += this.state.metrics[i].getMeanrunningtime();
        }
        lineGraphMetrics[0] = meanTime;
    }

    addData() {
        const chartData = BarGraphData;
        this.setState({chartData}, () => {
            this.state.updated = !this.state.updated;
        });
    }

    render() {
        return (
            <Wrapper store={store} component={
                <Grid container spacing={16} justify="center">
                    <Grid item xs={12} sm={6}>
                        <ReactChart.Line
                            data={LineGraphData}
                            width={width}
                            height={height}
                            options={{
                                maintainAspectRatio: true,
                                scales: {
                                    yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Time (s)'
                                        },
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                }
                            }}
                            redraw/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <ReactChart.Bar
                            data={BarGraphData}
                            width={width}
                            height={height}
                            options={{
                                maintainAspectRatio: true,
                                scales: {
                                    yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Number of Jobs'
                                        },
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                }
                            }}
                            redraw/>
                    </Grid>
                    {/* <Grid item>
                        <CircularProgress />
                    </Grid> */}
                </ Grid>}
            />
        );
    }
}
