import { Button, Card, Checkbox, FormControlLabel, FormGroup, TextField } from '@material-ui/core';
import * as React from 'react';

import { JobDefinition, JobDefinitionOption } from '../../generated/api/job_definition_pb';
import { buttonStyle, textFieldStyle } from './Definition';

interface EditingProps {
    definition: JobDefinition;
    save: (definition: JobDefinition) => void;
    cancel: () => void;
}

interface EditingState {
    definition: JobDefinition;
}

export class Editing extends React.Component<EditingProps, EditingState> {

    public readonly state: EditingState = {
        definition: this.props.definition.clone() as JobDefinition,
    }

    replaceOption(i: number, option: JobDefinitionOption, callback: (option: JobDefinitionOption) => JobDefinitionOption) {
        let definition = this.state.definition.clone() as JobDefinition;
        let updatedOptions = definition.getOptionsList();
        let newOption = option.clone() as JobDefinitionOption;

        updatedOptions[i] = callback(newOption);
        definition.setOptionsList(updatedOptions);
        this.setState({ definition: definition });
    }

    render() {
        return (
            <div id='editing'>
                <Card>
                    <form>
                        <FormGroup>
                            <TextField id="jobName" label="Job name" value={this.state.definition.getName()} style={textFieldStyle} onChange={(e) => {
                                let definition = this.state.definition.clone() as JobDefinition;
                                definition.setName(e.target.value);
                                this.setState({ definition: definition });
                            }} />
                        </FormGroup>
                        {
                            Object.values(this.state.definition.getOptionsList()).map((option: JobDefinitionOption, i: number) => {
                                return (
                                    <div>
                                        <FormGroup row>
                                            <TextField id="optionName" label="Option name" style={textFieldStyle} value={option.getName()} onChange={(e) => {
                                                this.replaceOption(i, option, (option: JobDefinitionOption) => {
                                                    option.setName(e.target.value);
                                                    return option;
                                                });
                                            }} />
                                            <TextField id="optionAllowed" label="Allowed values" style={textFieldStyle} value={option.getAllowedList()} onChange={(e) => {
                                                this.replaceOption(i, option, (option: JobDefinitionOption) => {
                                                    option.setAllowedList(e.target.value.split(","));
                                                    return option;
                                                });
                                            }} />
                                            <TextField id="optionDefault" label="Default value" style={textFieldStyle} value={option.getDefault()} onChange={(e) => {
                                                this.replaceOption(i, option, (option: JobDefinitionOption) => {
                                                    option.setDefault(e.target.value);
                                                    return option;
                                                });
                                            }} />
                                            <TextField id="optionDescription" label="Description" style={textFieldStyle} value={option.getDescription()} onChange={(e) => {
                                                let newOption = option.clone() as JobDefinitionOption;
                                                this.replaceOption(i, option, (option: JobDefinitionOption) => {
                                                    option.setDescription(e.target.value);
                                                    return option;
                                                });
                                            }} />

                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={option.getRequired()}
                                                        onChange={() => {
                                                            this.replaceOption(i, option, (option: JobDefinitionOption) => {
                                                                option.setRequired(!option.getRequired());
                                                                return option;
                                                            });
                                                        }}
                                                        value="checkedB"
                                                        color="primary"
                                                    />
                                                }
                                                label="Required"
                                            />
                                            <Button variant="raised" style={buttonStyle} onClick={(e) => {
                                                let definition = this.state.definition.clone() as JobDefinition;
                                                let options: Array<JobDefinitionOption> = [];
                                                for (let option of this.state.definition.getOptionsList()) {
                                                    if (option.getName() === option.getName()) {
                                                        continue;
                                                    }
                                                    options.push(option);
                                                }
                                                definition.setOptionsList(options);
                                                this.setState({ definition: definition });
                                            }}>Delete</Button>
                                        </FormGroup>
                                    </div>
                                );
                            })
                        }
                        <Button variant="raised" style={buttonStyle} onClick={(e) => {
                            let definition = this.state.definition.clone() as JobDefinition;
                            definition.addOptions(new JobDefinitionOption());
                            this.setState({ definition: definition });
                        }}>Add option</Button>
                        <br />
                        <FormGroup>
                            <TextField id="optionImage" label="Container image" margin="normal" style={textFieldStyle} value={this.state.definition.getImage()} onChange={(e) => {
                                let definition = this.state.definition.clone() as JobDefinition;
                                definition.setImage(e.target.value);
                                this.setState({ definition: definition });
                            }} />
                        </FormGroup>
                        <br />
                        <br />
                        <div>
                            <Button variant="raised" style={buttonStyle} color="primary" onClick={(e) => this.props.save(this.state.definition)}>Save</Button>
                            <Button variant="raised" style={buttonStyle} onClick={(e) => this.props.cancel()}>Cancel</Button>
                        </div>
                    </form>
                </Card>
            </div>
        );
    }
}
