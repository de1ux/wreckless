import { resolve } from 'dns';
import { configure, shallow } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';

import { ProjectViewer } from './ProjectViewer';

configure({ adapter: new Adapter() });

it('renders Jobs associated with current Project', () => {
    const creating = shallow(
        <ProjectViewer />)
    expect(creating.exists()).toBeTruthy();
});
