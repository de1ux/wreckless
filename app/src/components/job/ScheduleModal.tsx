import * as React from 'react';
import { buttonStyle } from './Definition';
import { Dialog, DialogTitle, DialogContent, TextField, DialogActions, Button, DialogContentText } from '@material-ui/core';
import cronstrue from 'cronstrue';

interface ScheduleModalProps {
    onClose: () => void;
    onSchedule: (cronTimeSchedule: string) => void;
    open: boolean;
}

interface ScheduleModalState {
    definition: string;
    valid: boolean;
    scheduledTime: string;
}

export class ScheduleModal extends React.Component<ScheduleModalProps, ScheduleModalState> {

    public readonly state: ScheduleModalState = {
        definition: "",
        valid: false,
        scheduledTime: "",
    }

    cronValidator(cronTime: string) {
        try {
            this.setState({
                scheduledTime: cronstrue.toString(cronTime),
                valid: true,
            })
        } catch (error) {
            this.setState({ valid: false });
        }
    }

    reset() {
        this.setState({
            definition: "",
            scheduledTime: "",
            valid: false,
        })
    }

    render() {
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={() => {
                        this.props.onClose();
                        this.reset();
                    }}
                >
                    <DialogTitle>Schedule Job</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            <a target="_blank" href={`https://crontab.guru/`}>
                                Link to Cron Time Generator
                        </a>
                        </DialogContentText>
                        <TextField
                            id="name"
                            label="Enter Cron Time"
                            value={this.state.definition}
                            onChange={(e) => {
                                let definition = e.target.value;
                                this.setState({ definition: definition });
                                this.cronValidator(definition);
                            }}
                        />
                        <DialogContentText>
                            {this.state.scheduledTime}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="raised" style={buttonStyle} color="secondary" disabled={!this.state.valid}
                            onClick={() => {
                                this.props.onSchedule(this.state.definition);
                                this.reset();
                                this.props.onClose();
                            }}>
                            Schedule
                    </Button>
                        <Button variant="raised" style={buttonStyle} onClick={() => {
                            this.reset();
                            this.props.onClose();
                        }}>
                            Cancel
                    </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}