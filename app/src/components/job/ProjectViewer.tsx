import * as React from 'react';
import { Store, Unsubscribe } from 'redux';
import { State, CreateGlobalStore } from '../Store';
import { Definition, View } from './Definition';
import Paper from '@material-ui/core/Paper';
import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { grpc } from 'grpc-web-client';
import { WrecklessService } from '../../generated/api/service_pb_service';
import { Empty, ReadProjectMessage, ReadJobByIdMessage } from '../../generated/api/service_pb';
import { JobDefinition } from '../../generated/api/job_definition_pb';
import { Sidebar } from '../../Sidebar';
import { Project } from '../../generated/api/project_pb';
import { getProjectID } from '../../Utilities';

let store: Store<State> = CreateGlobalStore();

interface ProjectViewerState {
    jobDefinitions: Array<JobDefinition>;
    projectInfo: Project;
    lastRefresh: string;
    open: boolean;
}

export class ProjectViewer extends React.Component<{}, ProjectViewerState> {
    unsubscribe: Unsubscribe;

    constructor(props: {}) {
        super(props);

        this.unsubscribe = store.subscribe(() => this.onStoreTrigger());
    }

    public readonly state: ProjectViewerState = {
        jobDefinitions: [],
        projectInfo: new Project,
        lastRefresh: '',
        open: false,
    }

    onStoreTrigger(): void {
        let nextState: State = store.getState();
        if (nextState.refreshJobs && this.state.lastRefresh !== nextState.refreshJobs.time) {
            this.setState({ lastRefresh: nextState.refreshJobs.time });
            this.renderDefinitions();
        }
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    componentDidMount() {
        this.renderDefinitions();
        this.getProjectInfo();
    }

    renderDefinitions() {
        // TODO - make these calls easier
        let jobsRequest = new ReadJobByIdMessage();
        jobsRequest.setId(getProjectID());
        let definitions: Array<JobDefinition> = [];
        grpc.invoke(WrecklessService.ReadJobsByProjectID, {
            request: jobsRequest,
            host: window.location.origin,
            onMessage: ((def: JobDefinition) => {
                definitions.push(def);
            }),
            onEnd: (res: any) => {
                this.setState({ jobDefinitions: definitions });
            }
        });
    }

    getProjectInfo() {
        let readMessage = new ReadProjectMessage();
        readMessage.setId(getProjectID());

        grpc.unary(WrecklessService.ReadProjectByID, {
            request: readMessage,
            host: window.location.origin,
            onEnd: (res) => {
                const { status, statusMessage, headers, message, trailers } = res;

                this.setState({ projectInfo: message as Project });
            }
        })
    }

    render() {
        return (
            <div style={{ flexGrow: 1 }}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton color="inherit" aria-label="Menu" onClick={() => {
                            this.setState({ open: true })
                        }}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" style={{ flex: 1 }}>
                            Wreckless > {this.state.projectInfo.getName()}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Sidebar open={this.state.open} onClose={() => {
                    this.setState({ open: false })
                }} />
                <Paper>
                    <Definition store={store} view={View.editing} key={'editing'} />
                </Paper>
                {
                    Object.values(this.state.jobDefinitions).map((jobDefinition: JobDefinition) => {
                        return (
                            <Definition store={store} jobDefinition={jobDefinition} key={jobDefinition.getKey()} />
                        );
                    })
                }
            </div>
        );
    }
}