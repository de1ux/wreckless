import { CircularProgress, Grid } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { grpc } from 'grpc-web-client';
import * as React from 'react';
import { Store, Unsubscribe } from 'redux';

import { JobDefinition } from '../../generated/api/job_definition_pb';
import { Empty } from '../../generated/api/service_pb';
import { WrecklessService } from '../../generated/api/service_pb_service';
import { State } from '../Store';
import { Definition, View } from './Definition';

interface DefinitionsViewerState {
    jobDefinitions: Array<JobDefinition>;
    lastRefresh: string;
}

interface DefinitionsViewerProps {
    store: Store<State>;
}

export class DefinitionsViewer extends React.Component<DefinitionsViewerProps, DefinitionsViewerState> {
    unsubscribe: Unsubscribe;

    public readonly state: DefinitionsViewerState = {
        jobDefinitions: [],
        lastRefresh: '',
    };

    constructor(props: DefinitionsViewerProps) {
        super(props);

        this.unsubscribe = this.props.store.subscribe(() => this.onStoreTrigger());
    }

    onStoreTrigger(): void {
        let nextState: State = this.props.store.getState();
        if (nextState.refreshJobs && this.state.lastRefresh !== nextState.refreshJobs.time) {
            this.setState({ lastRefresh: nextState.refreshJobs.time });
            this.renderDefinitions();
        }
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    componentDidMount() {
        this.renderDefinitions();
    }

    renderDefinitions() {
        // TODO - make these calls easier
        let definitions: Array<JobDefinition> = [];
        grpc.invoke(WrecklessService.GetJobDefinitions, {
            request: new Empty(),
            host: window.location.origin,
            onMessage: ((def: JobDefinition) => {
                definitions.push(def);
            }),
            onEnd: (res: any) => {
                this.setState({ jobDefinitions: definitions });
            }
        });
    }

    render() {
        return (
            <div>
                <Paper>
                    <Definition store={this.props.store} view={View.editing} key={'editing'} />
                </Paper>
                {
                    this.state.jobDefinitions.length > 0 ?
                        Object.values(this.state.jobDefinitions).map((jobDefinition: JobDefinition) => {
                            return (
                                <Definition store={this.props.store} jobDefinition={jobDefinition} key={jobDefinition.getKey()} />
                            );
                        }) :
                        <Grid container
                            spacing={16}
                            alignItems={"center"}
                            justify={"center"} >
                            <Grid item>
                                <CircularProgress />
                            </Grid>
                        </Grid >
                }
            </div >
        );
    }
}
