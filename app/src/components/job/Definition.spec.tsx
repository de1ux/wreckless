import { configure, mount, } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';

import { Definition, View } from './Definition';
import { Store } from '../../../node_modules/redux';
import { State, CreateGlobalStore } from '../Store';
import { JobDefinition } from '../../generated/api/job_definition_pb';
import { Execution } from '../../generated/api/execution_pb';

configure({ adapter: new Adapter() });
let store: Store<State> = CreateGlobalStore();

it('Should render correct component based on view', () => {
    const editing = mount(
        <Definition store={store} jobDefinition={new JobDefinition()} view={View.editing} />
    )
    expect(editing.find('#editing').exists()).toBeTruthy();

    const viewing = mount(
        <Definition store={store} jobDefinition={new JobDefinition()} view={View.viewing} />
    )
    expect(viewing.find('#viewing').exists()).toBeTruthy();

    const scheduling = mount(
        <Definition store={store} jobDefinition={new JobDefinition()} view={View.scheduling} />
    )
    expect(scheduling.find('#scheduling').exists()).toBeTruthy();

    // const running = mount(
    //     <Definition store={store} jobDefinition={new JobDefinition()} view={View.viewing} />
    // )
    // let a = running.instance() as Definition
    // let b = new Execution()
    // b.setScheduletype(0)
    // debugger;
    // a.run(b)
    // expect(running.find('#running').exists()).toBeTruthy();
}, 1000);