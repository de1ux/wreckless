import { grpc } from 'grpc-web-client';
import * as React from 'react';
import { Store } from 'redux';

import { Execution, ExecutionLog } from '../../generated/api/execution_pb';
import { JobDefinition, JobDefinitionOption } from '../../generated/api/job_definition_pb';
import { GetExecution } from '../../generated/api/service_pb';
import { WrecklessService } from '../../generated/api/service_pb_service';
import { GLOBAL_MESSAGE, REFRESH_JOBS, State } from '../Store';
import { Editing } from './Editing';
import { Running } from './Running';
import { Scheduling } from './Scheduling';
import { Viewing } from './Viewing';
import { getProjectID } from '../../Utilities';

export enum View {
    editing = 0,
    viewing,
    scheduling,
    running
}

export const textFieldStyle = {
    marginLeft: 10,
    marginRight: 10
};

export const buttonStyle = {
    marginLeft: 10,
    marginBottom: 10,
    marginTop: 10
};

export const definitionStyle = {
    marginBottom: 10
};

interface DefinitionState {
    jobDefinition: JobDefinition;
    execution?: Execution;
    view: View;
    log: string;
}

interface DefinitionProps {
    store: Store<State>;
    jobDefinition?: JobDefinition;
    view?: View;
}

export class Definition extends React.Component<DefinitionProps, DefinitionState> {

    public readonly state: DefinitionState = {
        view: this.props.view !== undefined ? this.props.view : View.viewing,
        jobDefinition: this.props.jobDefinition ? this.props.jobDefinition : new JobDefinition(),
        log: ''
    }

    get viewToRenderer(): React.ReactNode {
        switch (this.state.view) {
            case View.running: {
                return this.renderRunning();
            }
            case View.scheduling: {
                return this.renderScheduling();
            }
            case View.viewing: {
                return this.renderViewing();
            }
            default: {
                return this.renderEditing();
            }
        }
    }

    reset() {
        this.setState({
            view: this.props.view !== undefined ? this.props.view : View.viewing,
            jobDefinition: this.props.jobDefinition ? this.props.jobDefinition : this.createJobDefinition(),
            log: ''
        });
    }

    createJobDefinition() {
        let job = new JobDefinition()
        job.setProjectkey(getProjectID())
        return job
    }

    addOption() {
        let definition: JobDefinition = this.state.jobDefinition.clone() as JobDefinition;
        definition.addOptions(new JobDefinitionOption());
        this.setState({ jobDefinition: definition });
    }

    replaceOption(option: JobDefinitionOption) {
        let newOptions: Array<JobDefinitionOption> = [];
        let existingOptions: Array<JobDefinitionOption> = this.state.jobDefinition.getOptionsList()
        for (let i = 0; i < existingOptions.length; i++) {
            if (existingOptions[i].getName() === option.getName()) {
                newOptions.push(option);
            } else {
                newOptions.push(existingOptions[i]);
            }
        }

        let definition: JobDefinition = this.state.jobDefinition.clone() as JobDefinition;
        definition.setOptionsList(newOptions);
        this.setState({ jobDefinition: definition });
    }

    replaceDefinition(definition: JobDefinition) {
        this.setState({ jobDefinition: definition });
    }

    handleEditing() {
        this.setState({ view: View.editing });
    }

    cancel() {
        this.setState({ view: View.viewing });
    }

    save(definition: JobDefinition) {
        this.setState({ view: View.editing });

        definition.setProjectkey(getProjectID());

        // TODO - make these calls easier
        grpc.unary(WrecklessService.CreateOrUpdateJobDefinition, {
            request: definition,
            host: window.location.origin,
            onEnd: (res: any) => {
                const { status, statusMessage, headers, message, trailers } = res;

                this.props.store.dispatch({
                    type: GLOBAL_MESSAGE,
                    globalMessage: {
                        message: 'Saved job successfully!',
                        time: new Date().toISOString()
                    }
                });
                this.props.store.dispatch({
                    type: REFRESH_JOBS,
                    refreshJobs: {
                        time: new Date().toISOString()
                    }
                });
            }
        });

        this.reset();
    }

    delete() {
        // TODO - make these calls easier
        grpc.unary(WrecklessService.DeleteJobDefinition, {
            request: this.state.jobDefinition,
            host: window.location.origin,
            onEnd: (res: any) => {
                this.props.store.dispatch({
                    type: GLOBAL_MESSAGE,
                    globalMessage: {
                        message: 'Deleted job successfully!',
                        time: new Date().toISOString()
                    }
                });
                this.props.store.dispatch({
                    type: REFRESH_JOBS,
                    refreshJobs: {
                        time: new Date().toISOString()
                    }
                });
            }
        });
    }

    schedule() {
        this.setState({
            view: View.scheduling,
        });
    }

    stop() {
        throw new Error('Stop not implemented');
    }

    run(execution: Execution) {
        this.setState({ execution: execution });

        // TODO - make these calls easier
        new Promise((resolve, reject) => {
            grpc.unary(WrecklessService.ModifyExecutionFromExecutionId, {
                request: execution,
                host: window.location.origin,
                onEnd: (res) => {
                    const { status, statusMessage, headers, message, trailers } = res;
                    status === grpc.Code.OK ? resolve((message as Execution)) : reject();
                }
            });
        }).then((execution: Execution) => {
            if (execution.getScheduletype() === Execution.ScheduleType.LATER) {
                this.setState({
                    view: View.viewing,
                    execution: execution
                });
                this.props.store.dispatch({
                    type: GLOBAL_MESSAGE,
                    globalMessage: {
                        message: 'Scheduled job successfully!',
                        time: new Date().toISOString()
                    }
                });

            } else if (execution.getScheduletype() === Execution.ScheduleType.NOW) {
                this.setState({
                    view: View.running,
                    execution: execution
                });

                let buffer = '';
                grpc.invoke(WrecklessService.GetExecutionLogsFromExecution, {
                    request: execution,
                    host: window.location.origin,
                    onMessage: ((log: ExecutionLog) => {
                        console.log("Got log: " + log.getLog());
                        buffer += log.getLog();

                        if (buffer.indexOf('\\n') !== -1) {
                            this.setState({ log: this.state.log + buffer });
                            buffer = '';
                        }
                    }),
                    onEnd: (res: any) => {
                        let getExecution = new GetExecution();
                        getExecution.setId(this.state.execution!!.getKey());

                        grpc.unary(WrecklessService.GetExecutionFromExecutionId, {
                            request: getExecution,
                            host: window.location.origin,
                            onEnd: (res) => {
                                const { status, statusMessage, headers, message, trailers } = res;

                                this.setState({ execution: message as Execution });
                            }
                        })
                    }
                });

            }
        }).catch((e) => {
            this.props.store.dispatch({
                type: GLOBAL_MESSAGE,
                globalMessage: {
                    message: e,
                    time: new Date().toISOString()
                }
            });
        });
    }

    renderEditing() {
        return <Editing
            key={this.state.jobDefinition.getKey()}
            definition={this.state.jobDefinition}
            save={(definition: JobDefinition) => this.save(definition)}
            cancel={() => this.cancel()} />;
    }

    renderViewing() {
        return <Viewing
            key={this.state.jobDefinition.getKey()}
            definition={this.state.jobDefinition}
            handleEditing={() => this.handleEditing()}
            handleDelete={() => this.delete()}
            handleScheduling={() => this.schedule()} />;
    }

    renderScheduling() {
        return <Scheduling
            key={this.state.jobDefinition.getKey()}
            definition={this.state.jobDefinition}
            run={(execution: Execution) => this.run(execution)}
            cancel={() => this.cancel()}
            handleEditing={() => this.handleEditing()}
            handleDelete={() => this.delete()}
            handleScheduling={() => this.schedule()} />;
    }

    renderRunning() {
        return <Running
            key={this.state.execution!!.getKey()}
            execution={this.state.execution!!}
            definition={this.state.jobDefinition}
            log={this.state.log}
            stop={() => this.stop()}
            handleEditing={() => this.handleEditing()}
            handleDelete={() => this.delete()}
            handleScheduling={() => this.schedule()} />;
    }

    render() {
        return <div style={definitionStyle}>{this.viewToRenderer}</div>;
    }
}
