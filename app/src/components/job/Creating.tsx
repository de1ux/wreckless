import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@material-ui/core';
import * as React from 'react';

import { Project } from '../../generated/api/project_pb';
import { buttonStyle } from './Definition';

interface CreatingProps {
    onClose: () => void;
    onCreate: (p: Project) => void;
    open: boolean
}

interface CreatingStates {
    definition: Project;
}

export class Creating extends React.Component<CreatingProps, CreatingStates> {
    public readonly state: CreatingStates = {
        definition: new Project()
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={this.props.onClose}
            >
                <DialogTitle>Create New Project</DialogTitle>
                <DialogContent>
                    <TextField
                        id="name"
                        label="Project Name"
                        value={this.state.definition.getName()}
                        onChange={(e) => {
                            let definition = this.state.definition.clone() as Project;
                            definition.setName(e.target.value);
                            this.setState({ definition: definition });
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button id="create" variant="raised" style={buttonStyle} color="primary" onClick={() => {
                        this.props.onCreate(this.state.definition);
                    }}>
                        Create
                    </Button>
                    <Button id="cancel" variant="raised" style={buttonStyle} onClick={this.props.onClose}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}