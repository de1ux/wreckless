import { Button, Card, CardContent, CardHeader, IconButton, Menu, MenuItem, Paper, Typography } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import * as React from 'react';

import { Execution } from '../../generated/api/execution_pb';
import { JobDefinition } from '../../generated/api/job_definition_pb';

interface RunningState {
    anchorEl: any;
}

interface RunningProps {
    log: string;
    definition: JobDefinition;
    execution: Execution;
    stop: () => void;
    handleDelete: () => void;
    handleScheduling: () => void;
    handleEditing: () => void;
}

const logViewerStyleBase = {
    fontFamily: 'monospace',
    color: 'white',
    padding: 10,
};

const logViewerStyleRunning = {
    backgroundColor: '#38315d'
};

const logViewerStyleSucceeded = {
    backgroundColor: '#429245'
};

const logViewerStyleFailed = {
    backgroundColor: '#924242'
};

export class Running extends React.Component<RunningProps, RunningState> {
    get log(): JSX.Element {
        return <div>
            {
                Object.values(this.props.log.split('\\n')).map((line: string, i: number) => {
                    return <p key={`log-${i}`}>{line}</p>;
                })
            }
        </div>;
    }

    get stylesByStatus(): React.CSSProperties {
        if (this.props.execution.getStatus() === Execution.Status.SUCCEEDED) {
            return { ...logViewerStyleBase, ...logViewerStyleSucceeded }
        } else if (this.props.execution.getStatus() === Execution.Status.FAILED) {
            return { ...logViewerStyleBase, ...logViewerStyleFailed };
        } else {
            return { ...logViewerStyleBase, ...logViewerStyleRunning };
        }
    }

    public readonly state: RunningState = {
        anchorEl: null
    }

    handleOpenEditControl = (event: any) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleCloseEditControl = () => {
        this.setState({ anchorEl: null });
    }

    render() {
        return (
            <Card id="running">
                <CardHeader
                    action={
                        <div>
                            <IconButton aria-owns={this.state.anchorEl != null ? 'simple-menu' : undefined}
                                aria-haspopup="true"
                                onClick={(e) => this.handleOpenEditControl(e)}>
                                <MoreVertIcon />
                            </IconButton>
                            <Menu id="simple-menu"
                                open={this.state.anchorEl != null}
                                anchorEl={this.state.anchorEl}
                                onClose={() => this.handleCloseEditControl()}>
                                <MenuItem onClick={(e) => {
                                    this.handleCloseEditControl();
                                    // this.props.handleEditing();
                                }}>Edit</MenuItem>
                                <MenuItem onClick={(e) => {
                                    this.handleCloseEditControl();
                                    // this.props.delete();
                                }}>Delete</MenuItem>
                                <MenuItem onClick={(e) => {
                                    this.handleCloseEditControl();
                                    // this.props.schedule();
                                }}>Run</MenuItem>
                            </Menu>
                        </div>
                    }
                    title={this.props.definition.getName()}
                    subheader={this.props.definition.getImage()}
                />
                <CardContent>
                    <Typography id="image" variant="subheading" color="textSecondary">Logs</Typography>
                    <Paper style={this.stylesByStatus}>
                        {this.log}
                        <Button id="stop" onClick={() => this.props.stop()}>Stop</Button>
                    </Paper>
                </CardContent>
            </Card>
        );
    }
}