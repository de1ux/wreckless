import { configure, shallow } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';

import { Project } from '../../generated/api/project_pb';
import { Creating } from './Creating';

configure({ adapter: new Adapter() });

it('renders a Creating modal', () => {
    const creating = shallow(
        <Creating onCreate={(p: Project) => { }} onClose={() => { }} open={true} />)
    expect(creating.exists()).toBeTruthy();
});

it('triggers onClose', () => {
    return new Promise((resolve, reject) => {
        const creating = shallow(
            <Creating onCreate={(p: Project) => {
                reject();
            }} onClose={() => {
                resolve();
            }} open={true} />);

        creating.find('#cancel').simulate('click');
    });
}, 1000);

it('triggers onCreate', () => {
    return new Promise((resolve, reject) => {
        const creating = shallow(
            <Creating onCreate={(p: Project) => {
                resolve();
            }} onClose={() => {
                reject()
            }} open={true} />);

        creating.find('#create').simulate('click');
    });
}, 1000);

it('sets a project name onChange', () => {
    return new Promise((resolve, reject) => {
        const creating = shallow(
            <Creating onCreate={(p: Project) => {
                if (p.getName() === 'test project') {
                    resolve();
                } else {
                    reject();
                }
            }} onClose={() => {
                reject();
            }} open={true} />);

        creating.find('#name').simulate('change', { target: { value: 'test project' } });
        creating.find('#create').simulate('click');
    });
}, 1000);
