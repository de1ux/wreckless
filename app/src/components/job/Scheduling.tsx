import {
    Button,
    Card,
    CardContent,
    CardHeader,
    Checkbox,
    FormControl,
    FormControlLabel,
    IconButton,
    Menu,
    MenuItem,
    Select,
    TextField,
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { grpc } from 'grpc-web-client';
import * as React from 'react';

import { Execution, ExecutionOption } from '../../generated/api/execution_pb';
import { JobDefinition, JobDefinitionOption } from '../../generated/api/job_definition_pb';
import { WrecklessService } from '../../generated/api/service_pb_service';
import { buttonStyle, textFieldStyle } from './Definition';
import { ScheduleModal } from './ScheduleModal';

interface SchedulingState {
    anchorEl: any;
    execution: Execution;
    open: boolean;
}

interface SchedulingProps {
    definition: JobDefinition;
    run: (execution: Execution) => void;
    cancel: () => void;
    handleDelete: () => void;
    handleScheduling: () => void;
    handleEditing: () => void;
}

export class Scheduling extends React.Component<SchedulingProps, SchedulingState> {

    public readonly state: SchedulingState = {
        anchorEl: null,
        execution: new Execution(),
        open: false,
    }

    componentWillMount() {
        let execution = new Execution();
        execution.setStatus(0);
        execution.setScheduletype(0);
        execution.setJobdefinitionkey(this.props.definition.getKey());

        for (let option of this.props.definition.getOptionsList()) {
            let executionOption = new ExecutionOption();
            executionOption.setJobdefinitionoptionkey(option.getKey());
            executionOption.setValue(option.getDefault());
            execution.addOptions(executionOption);
        }
        this.setState({
            execution: execution
        })
    }

    componentDidMount() {
        // TODO - make these calls easier
        grpc.unary(WrecklessService.CreateExecutionFromDefinitionId, {
            request: this.state.execution,
            host: window.location.origin,
            onEnd: (res: any) => {
                const { status, statusMessage, headers, message, trailers } = res;
                this.setState({ execution: message });
            }
        });
    }

    handleOpenEditControl = (event: any) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleCloseEditControl = () => {
        this.setState({ anchorEl: null });
    }

    handleRun() {
        // TODO - client side validation of requiredness needs to be expanded
        let execution = this.state.execution.clone() as Execution;
        execution.setScheduletype(Execution.ScheduleType.NOW);
        this.props.run(execution);
    }

    handleSchedule(cronTimeSchedule: string) {
        let execution = this.state.execution.clone() as Execution;
        execution.setScheduletype(Execution.ScheduleType.LATER);
        execution.setSchedulevalue(cronTimeSchedule);
        this.props.run(execution);
    }

    getJobOptionByJobOptionKey(key: string): JobDefinitionOption | null {
        for (let jobOption of this.props.definition.getOptionsList()) {
            if (jobOption.getKey() === key) {
                return jobOption;
            }
        }
        return null;
    }

    // replaceExecutionOption rebuilds the execution options without directly modifying
    // stateful execution options
    replaceExecutionOption(replacementOption: ExecutionOption) {
        let newOptions: Array<ExecutionOption> = [];
        let execution = this.state.execution.clone() as Execution;

        for (let executionOption of execution.getOptionsList()) {
            if (executionOption.getJobdefinitionoptionkey() === replacementOption.getJobdefinitionoptionkey()) {
                newOptions.push(replacementOption);
            } else {
                newOptions.push(executionOption);
            }
        }
        execution.setOptionsList(newOptions);

        this.setState({ execution: execution });
    }

    render() {
        let components = [];
        for (let execOption of this.state.execution.getOptionsList()) {

            let allowedOptions: Array<JSX.Element> = [];
            let manualOption: Array<JSX.Element> = [];

            let jobOption = this.getJobOptionByJobOptionKey(execOption.getJobdefinitionoptionkey())!!;
            if (jobOption.getAllowedList().length > 0) {
                // Push an empty selection
                allowedOptions.push(<option />);

                Object.values(jobOption.getAllowedList()).map((option: string) => {
                    allowedOptions.push(<option value={option}>{option}</option>);
                });

                allowedOptions = [
                    <Select native id="executionValues" value={execOption.getValue()} onChange={(e) => {
                        let newOption = execOption.clone() as ExecutionOption;
                        newOption.setValue(e.target.value);
                        this.replaceExecutionOption(newOption);
                    }}>
                        {allowedOptions}
                    </Select>
                ];
            } else {
                manualOption.push(<TextField value={execOption.getValue()} onChange={(e) => {
                    let newOption = execOption.clone() as ExecutionOption;
                    newOption.setValue(e.target.value);
                    this.replaceExecutionOption(newOption);
                }} />);
            }

            components.push(
                <div>
                    <TextField
                        disabled
                        id="executionName"
                        label="Name"
                        style={textFieldStyle}
                        value={jobOption.getName()} />
                    {allowedOptions}
                    {manualOption}
                    <TextField
                        disabled
                        id="optionDescription"
                        label="Description"
                        style={textFieldStyle}
                        value={jobOption.getDescription()} />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={jobOption.getAllowedList() !== null && jobOption.getAllowedList().length > 0 ? true : false}
                                disabled
                                value="checkedB"
                                color="primary"
                            />
                        }
                        label="Required"
                    />
                </div>
            );
        }

        return (
            <Card id="scheduling">
                <CardHeader
                    action={
                        <div>
                            <IconButton aria-owns={this.state.anchorEl != null ? 'simple-menu' : undefined}
                                aria-haspopup="true"
                                onClick={(e) => this.handleOpenEditControl(e)}>
                                <MoreVertIcon />
                            </IconButton>
                            <Menu id="simple-menu"
                                open={this.state.anchorEl != null}
                                anchorEl={this.state.anchorEl}
                                onClose={() => this.handleCloseEditControl()}>
                                <MenuItem onClick={(e) => {
                                    this.handleCloseEditControl();
                                    this.props.handleEditing();
                                }}>Edit</MenuItem>
                                <MenuItem onClick={(e) => {
                                    this.handleCloseEditControl();
                                    this.props.handleDelete();
                                }}>Delete</MenuItem>
                                <MenuItem onClick={(e) => {
                                    this.handleCloseEditControl();
                                    this.props.handleScheduling();
                                }}>Run</MenuItem>
                            </Menu>
                        </div>
                    }
                    title={this.props.definition.getName()}
                    subheader={this.props.definition.getImage()}
                />
                <CardContent>
                    <form>
                        <FormControl>
                            {components}
                            <ScheduleModal open={this.state.open}
                                onClose={() => { this.setState({ open: false }) }}
                                onSchedule={(cronTimeSchedule: string) => this.handleSchedule(cronTimeSchedule)}
                            />
                            <div>
                                <Button variant="raised" style={buttonStyle} color="primary" onClick={(e) => this.handleRun()}>Run</Button>
                                <Button variant="raised" style={buttonStyle} color="secondary" onClick={() => {
                                    this.setState({ open: true })
                                }}>Schedule</Button>
                                <Button variant="raised" style={buttonStyle} onClick={(e) => this.props.cancel()}>Cancel</Button>
                            </div>
                        </FormControl>
                    </form>
                </CardContent>
            </Card>
        );
    }
}