import { configure, shallow } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';

import { Execution } from '../../generated/api/execution_pb';
import { JobDefinition } from '../../generated/api/job_definition_pb';
import { Running } from './Running';

configure({ adapter: new Adapter() });

it('renders a Running component', () => {
    const running = shallow(
        <Running log={''}
            definition={new JobDefinition()}
            execution={new Execution()}
            stop={() => { }}
            handleDelete={() => { }}
            handleEditing={() => { }}
            handleScheduling={() => { }} />);
    expect(running.exists()).toBeTruthy();
});

it('triggers stop', () => {
    return new Promise((resolve, reject) => {
        const running = shallow(
            <Running log={''}
                definition={new JobDefinition()}
                execution={new Execution()}
                stop={() => {
                    resolve();
                }}
                handleDelete={() => { }}
                handleEditing={() => { }}
                handleScheduling={() => { }} />);

        running.find('#stop').simulate('click');
    });
}, 1000);

it('displays the log', () => {
    const running = shallow(
        <Running log={'test log 1\\ntest log 2'}
            definition={new JobDefinition()}
            execution={new Execution()}
            stop={() => { }}
            handleDelete={() => { }}
            handleEditing={() => { }}
            handleScheduling={() => { }} />);

    expect(running.contains(<p>test log 1</p>)).toBeTruthy();
    expect(running.contains(<p>test log 2</p>)).toBeTruthy();
}, 1000);
