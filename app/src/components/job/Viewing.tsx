import { Card, CardContent, CardHeader, IconButton, Menu, MenuItem, Typography } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import * as React from 'react';

import { JobDefinition } from '../../generated/api/job_definition_pb';

interface ViewingState {
    anchorEl: any;
}

interface ViewingProps {
    definition: JobDefinition;
    handleDelete: () => void;
    handleScheduling: () => void;
    handleEditing: () => void;
}

export class Viewing extends React.Component<ViewingProps, ViewingState> {

    public readonly state: ViewingState = {
        anchorEl: null
    }

    handleOpenEditControl = (event: any) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleCloseEditControl = () => {
        this.setState({ anchorEl: null });
    }

    render() {
        return (
            <Card id="viewing">
                <CardHeader
                    action={
                        <div>
                            <IconButton aria-owns={this.state.anchorEl != null ? 'simple-menu' : undefined}
                                aria-haspopup="true"
                                onClick={(e) => this.handleOpenEditControl(e)}>
                                <MoreVertIcon />
                            </IconButton>
                            <Menu id="simple-menu"
                                open={this.state.anchorEl != null}
                                anchorEl={this.state.anchorEl}
                                onClose={() => this.handleCloseEditControl()}>
                                <MenuItem onClick={(e) => {
                                    this.handleCloseEditControl();
                                    this.props.handleEditing();
                                }}>Edit</MenuItem>
                                <MenuItem onClick={(e) => {
                                    this.handleCloseEditControl();
                                    this.props.handleDelete();
                                }}>Delete</MenuItem>
                                <MenuItem onClick={(e) => {
                                    this.handleCloseEditControl();
                                    this.props.handleScheduling();
                                }}>Run</MenuItem>

                            </Menu>
                        </div>
                    }
                    title={this.props.definition.getName()}
                    subheader={this.props.definition.getImage()}
                />
                <CardContent>
                    <Typography id="image" variant="subheading" color="textSecondary">More details</Typography>
                </CardContent>
            </Card>
        );
    }
}