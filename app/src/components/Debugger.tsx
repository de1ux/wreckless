import * as React from 'react';
import { Store, Unsubscribe } from 'redux';
import { GLOBAL_MESSAGE, REFRESH_JOBS, State } from './Store';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import BugReportIcon from '@material-ui/icons/BugReport';
import CloudQueueIcon from '@material-ui/icons/CloudQueue';


interface DebuggerProps {
    store: Store<State>;
}

interface DebuggerState {}

export class Debugger extends React.Component<DebuggerProps, DebuggerState> {
    unsubscribe: Unsubscribe;

    constructor(props: DebuggerProps) {
        super(props);

        this.unsubscribe = this.props.store.subscribe(() => this.onStoreTrigger());
    }


    loadDemoJobs() {
        fetch('api/v1/job', {
            method: 'POST',
            body: JSON.stringify({
                name: "Demo Job",
                image: "registry.sofi.com/streamer:deleteme",
                options: [
                    {
                        name: "optionA",
                        required: true,
                        description: "An optionA",
                        default: "defaultValue",
                        allowed: ["defaultValueA", "defaultValueB", "defaultValueC"]
                    }
                ]
            }),
            credentials: 'include'
        }).then((json) => {
            this.props.store.dispatch({
                type: REFRESH_JOBS,
                refreshJobs: {
                    time: new Date().toISOString()
                }
            })
        });
    }

    private onStoreTrigger(): void {
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    render() {
        return (
            <BottomNavigation showLabels>
                <BottomNavigationAction onClick={(e) => this.loadDemoJobs()} label="Load demo jobs" icon={<BugReportIcon />} />
                <BottomNavigationAction label="Running jobs" icon={<CloudQueueIcon />} />
            </BottomNavigation>
        );
    }
}
