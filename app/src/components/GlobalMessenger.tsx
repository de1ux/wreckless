import Snackbar from '@material-ui/core/Snackbar';
import * as React from 'react';
import { Store, Unsubscribe } from 'redux';

import { State } from './Store';

interface GlobalMessengerProps {
    store: Store<State>;
}

interface GlobalMessengerState {
    display: boolean;
    message: string;
}

export class GlobalMessenger extends React.Component<GlobalMessengerProps, GlobalMessengerState> {
    unsubscribe: Unsubscribe;

    constructor(props: GlobalMessengerProps) {
        super(props);
        this.unsubscribe = this.props.store.subscribe(() => this.onStoreTrigger());
    }
    
    public readonly state: GlobalMessengerState = {
        display: false,
        message: ''
    }

    onStoreTrigger(): void {
        let nextState: State = this.props.store.getState();
        this.setState({ message: nextState.globalMessage.message, display: true });
        setTimeout(() => {
            this.setState({ display: false })
        }, 5000);
    }

    getMessage() {
        return <span>{this.state.message}</span>;
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    render() {
        return (
            <Snackbar
                anchorOrigin={{horizontal:'right', vertical:'top'}}
                open={this.state.display}
                ContentProps={{ 'aria-describedby': 'message-id' }}
                message={this.getMessage()}
            />
        );
    }
}
