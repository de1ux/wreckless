import * as React from 'react';

import { Dashboard } from '../Dashboard';
import { Execution } from '../Execution';
import createMemoryHistory from 'history/createMemoryHistory';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ProjectViewer } from './job/ProjectViewer';
import { DefinitionsViewer } from './job/DefinitionsViewer';

export const Router: React.StatelessComponent<{}> = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Dashboard} />
                <Route path="/project" component={ProjectViewer} />
            </Switch>
        </BrowserRouter>
    );
}