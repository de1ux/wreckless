import { Button, Divider, Drawer, List, ListItem, ListItemText } from '@material-ui/core';
import { grpc } from 'grpc-web-client';
import * as React from 'react';

import { WrecklessService } from '../src/generated/api/service_pb_service';
import { Creating } from './components/job/Creating';
import { Project } from './generated/api/project_pb';
import { Empty } from './generated/api/service_pb';

interface SidebarProps {
    onClose: () => void
    open: boolean
};

interface SidebarState {
    projects: Array<Project>
    open: boolean
    projectDefinition: Project
}

export interface NewProject {
    name: string
    key: string
}

export class Sidebar extends React.Component<SidebarProps, SidebarState>{
    public readonly state: SidebarState = {
        projects: [],
        open: false,
        projectDefinition: new Project(),
    }

    createProject(definition: Project) {
        grpc.unary(WrecklessService.CreateProject, {
            request: definition,
            host: window.location.origin,
            onEnd: (res: any) => {
                const { status, statusMessage, headers, message, trailers } = res;

                this.setState({
                    projects: this.state.projects.concat(message),
                    open: false
                });
            }
        });
    }

    componentDidMount() {
        let definitions: Array<Project> = [];
        grpc.invoke(WrecklessService.GetProjects, {
            request: new Empty(),
            host: window.location.origin,
            onMessage: ((def: Project) => {
                definitions.push(def);
            }),
            onEnd: (res: any) => {
                this.setState({ projects: definitions });
            }
        });
    }

    render() {
        var elements: Array<JSX.Element> = [];
        for (var i = 0; i < this.state.projects.length; i++) {
            elements.push(
                <ListItem button component='a' href={`/project/${this.state.projects[i].getKey()}`}>
                    <ListItemText primary={this.state.projects[i].getName()} />
                </ListItem>
            );
        }

        return (
            <Drawer open={this.props.open} onClose={this.props.onClose}>
                <Button onClick={() => {
                    this.setState({ open: true })
                }}>
                    Create New Project
                        </Button>
                <Divider />
                <List>{elements}</List>
                <Creating open={this.state.open}
                    onClose={() => {
                        this.setState({ open: false })
                    }}
                    onCreate={(p: Project) => this.createProject(p)}
                />
            </Drawer>
        );
    }
}