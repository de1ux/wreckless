import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Router } from './src/components/Router';

ReactDOM.render(
  <Router />,
  document.getElementById('root')
);
