package listeners

import (
	"fmt"
	"time"

	model "gitlab.com/de1ux/wreckless/generated/api"
	"gitlab.com/de1ux/wreckless/persistence"
)

type metricsListener struct {
	persistor persistence.Persistor
}

type statistic struct {
	status model.Execution_Status
	start  float64
	end    float64
}

func newMetricsListener() *metricsListener {
	p, err := persistence.NewDatastorePersistor()
	if err != nil {
		panic(err)
	}

	return &metricsListener{p}
}

func (k *metricsListener) generateMetric(stats []*statistic, key string) *model.Metrics {
	metrics, err := k.persistor.GetMetrics()
	if err != nil {
		return nil
	}
	totalTime := float64(0)
	totalStats := float64(len(stats))
	metric := model.Metrics{}
	for _, stat := range stats {
		if stat.status == model.Execution_CREATED {
			metric.NumCreated++
		} else if stat.status == model.Execution_RUNNING {
			metric.NumRunning++
		} else if stat.status == model.Execution_SUCCEEDED {
			metric.NumSucceeded++
		} else if stat.status == model.Execution_FAILED {
			metric.NumFailed++
		} else if stat.status == model.Execution_ERRORED {
			metric.NumErrored++
		}
		totalTime += stat.end - stat.start
	}
	metric.MeanRunningTime = totalTime / totalStats
	metric.ProjectKey = key
	for _, met := range metrics {
		if met.ProjectKey == key {
			metric.Key = met.Key
		}
	}
	return &metric
}

func (k *metricsListener) Run() {
	for {
		pKeys, err := k.persistor.GetProjectKeys()
		if err != nil {
			panic(fmt.Errorf("Failed to get Project Keys: %s", err))
		}

		for _, pKey := range pKeys {
			executions, err := k.persistor.GetExecutionsFromProjectKey(pKey.Encode())
			if err != nil {
				panic(fmt.Errorf("Failed to get all Executions: %s", err))
			}
			stat := make([]*statistic, len(executions))
			for i, execution := range executions {
				stat[i] = &statistic{execution.Status, float64(execution.Started), float64(execution.Ended)}
			}
			_, err = k.persistor.CreateOrUpdateMetrics(k.generateMetric(stat, pKey.Encode()))
			if err != nil {
				panic(fmt.Errorf("Failed to CreateOrUpdateMetric: %s", err))
			}
		}
		time.Sleep(10 * time.Second)
	}
}
