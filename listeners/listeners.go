package listeners

import (
	"fmt"
)

func Run() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("Listener failed: %s", r)
		}
	}()

	go newKubernetesListener().Run()
	go newMetricsListener().Run()
}
