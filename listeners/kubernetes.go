package listeners

import (
	"fmt"

	model "gitlab.com/de1ux/wreckless/generated/api"
	"gitlab.com/de1ux/wreckless/orchestration"
	"gitlab.com/de1ux/wreckless/persistence"

	batchv1 "k8s.io/api/batch/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type kubernetesListener struct {
	persistor persistence.Persistor
	client    *kubernetes.Clientset
}

func newKubernetesListener() *kubernetesListener {
	p, err := persistence.NewDatastorePersistor()
	if err != nil {
		panic(err)
	}

	c, err := orchestration.InitializeKubernetesClient()
	if err != nil {
		panic(err)
	}

	return &kubernetesListener{p, c}
}

// TODO - add a cloudstore index on Status to only update
func (k *kubernetesListener) Run() {
	for {
		jobs, err := k.client.BatchV1().Jobs("default").List(metav1.ListOptions{})
		if err != nil {
			panic(err)
		}

		errorChan := make(chan error, len(jobs.Items))

		for _, job := range jobs.Items {
			go func(persistor persistence.Persistor, job batchv1.Job, errorChan chan error) {
				println(job.ObjectMeta.Name)
				// Not finished or newly created
				if job.Status.StartTime == nil {
					errorChan <- nil
					return
				}

				completionTime := job.Status.CompletionTime
				if completionTime == nil {
					for _, condition := range job.Status.Conditions {
						if condition.Reason == "BackoffLimitExceeded" {
							completionTime = &condition.LastTransitionTime
							break
						}
					}
				}

				if completionTime == nil {
					errorChan <- nil
					return
				}
				println(job.ObjectMeta.Name + " has a time")

				execution, err := persistor.GetExecutionFromExecutionHash(job.ObjectMeta.Name)
				if err != nil {
					errorChan <- fmt.Errorf("Coming from GetExecution: %s", err)
					return
				}

				execution.Started = job.Status.StartTime.Time.Unix()
				execution.Ended = completionTime.Time.Unix()
				if job.Status.Succeeded > 0 {
					execution.Status = model.Execution_SUCCEEDED
				} else {
					execution.Status = model.Execution_FAILED
				}

				errorChan <- k.persistor.UpdateExecution(execution)
			}(k.persistor, job, errorChan)

		}

		for i := 0; i < len(jobs.Items); i++ {
			err := <-errorChan
			if err != nil {
				panic(err)
			}
		}
	}
}
