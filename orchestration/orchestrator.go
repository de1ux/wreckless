package orchestration

import (
	model "gitlab.com/de1ux/wreckless/generated/api"
)

type Orchestrator interface {
	GetJobs() (interface{}, error)
	RunJobToCompletion(string, string) error
	ScheduleJob(string, string) error
	StartJobs() error
	StopJobs() error
	GetLogs(*model.Execution) (<-chan byte, error)
}
