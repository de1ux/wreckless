package orchestration

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	model "gitlab.com/de1ux/wreckless/generated/api"

	vaultAPI "github.com/hashicorp/vault/api"
	batchv1 "k8s.io/api/batch/v1"
	batchv1beta1 "k8s.io/api/batch/v1beta1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/clientcmd"
)

type v1 struct {
	client *kubernetes.Clientset
}

var (
	environment        = os.Getenv("ENVIRONMENT")
	minikubeMode       = os.Getenv("DEBUG") != ""
	kubeVaultPath      = ""
	getLogsInterval    = time.Duration(time.Second * 2)
	getLogsAttempts    = 15
	getJobPodsInterval = time.Duration(time.Second * 2)
	getJobPodsAttempts = 15

	// Mockable for easy unit testing
	internalGetJobPods         = getJobPods
	internalGetJobPodLogStream = getJobPodLogStream
)

type chanWriter struct {
	ch chan byte
}

func newChanWriter() *chanWriter {
	return &chanWriter{make(chan byte, 1024)}
}

func (w *chanWriter) Chan() <-chan byte {
	return w.ch
}

func (w *chanWriter) Write(p []byte) (int, error) {
	n := 0
	for _, b := range p {
		w.ch <- b
		n++
	}
	return n, nil
}

func (w *chanWriter) Close() error {
	close(w.ch)
	return nil
}

func NewKubernetes() (Orchestrator, error) {
	client, err := InitializeKubernetesClient()
	if err != nil {
		return nil, err
	}

	return &v1{client}, nil
}

func getJobPods(client *kubernetes.Clientset, name string) ([]corev1.Pod, error) {
	pods, err := client.CoreV1().Pods("default").List(metav1.ListOptions{LabelSelector: fmt.Sprintf("job-name=%s", name)})
	return pods.Items, err
}

func getJobPodLogStream(client *kubernetes.Clientset, name string) (io.ReadCloser, error) {
	return client.
		RESTClient().
		Get().
		Prefix("api/v1").
		Namespace("default").
		Name(name).
		Resource("pods").
		SubResource("log").
		Param("follow", "true").
		Param("timestamps", "true").
		Stream()
}

func (o *v1) GetLogs(execution *model.Execution) (<-chan byte, error) {
	tries := 0

	items, err := internalGetJobPods(o.client, execution.Hash)
	for true {
		if err != nil {
			return nil, fmt.Errorf("Failed to list jobs: %s", err)
		}

		if tries > getLogsAttempts {
			return nil, fmt.Errorf("Failed to list jobs: retries exceeded")
		}

		if len(items) > 0 {
			break
		}

		tries += 1
		time.Sleep(getLogsInterval)
		items, err = internalGetJobPods(o.client, execution.Hash)
	}

	w := newChanWriter()

	tries = 0
	for true {
		if tries > getJobPodsAttempts {
			return nil, fmt.Errorf("Failed to get job logs: retries exceeded")
		}

		r, err := internalGetJobPodLogStream(o.client, items[0].ObjectMeta.Name)
		if err != nil {
			if r != nil {
				r.Close()
			}
			tries += 1
			time.Sleep(getJobPodsInterval)
			continue
		}

		go func(w *chanWriter, r io.ReadCloser) {
			if _, err := io.Copy(w, r); err != nil {
				panic(err)
			}
			r.Close()
			w.Close()
		}(w, r)

		break
	}

	return w.Chan(), nil
}

func (o *v1) GetJobs() (interface{}, error) {
	return o.client.BatchV1().Jobs("default").List(metav1.ListOptions{})
}

func (o *v1) RunJobToCompletion(jobName, jobSpec string) error {
	decode := scheme.Codecs.UniversalDeserializer().Decode
	obj, _, err := decode([]byte(jobSpec), nil, nil)
	if err != nil {
		return err
	}

	job, ok := obj.(*batchv1.Job)
	if !ok {
		return fmt.Errorf("Failed to coerce manifest to a batch job object")
	}

	println(fmt.Sprintf("Job name is %s", jobName))

	_, err = o.client.BatchV1().Jobs("default").Create(job)

	return err
}

func (o *v1) ScheduleJob(jobName, jobSpec string) error {
	decode := scheme.Codecs.UniversalDeserializer().Decode
	obj, _, err := decode([]byte(jobSpec), nil, nil)
	if err != nil {
		return err
	}

	job, ok := obj.(*batchv1beta1.CronJob)
	if !ok {
		return fmt.Errorf("Failed to coerce manifest to a batch cronjob object")
	}

	_, err = o.client.BatchV1beta1().CronJobs("default").Create(job)

	return err
}

func (o *v1) StartJobs() error {
	return nil
}

func (o *v1) StopJobs() error {
	return nil
}

func InitializeKubernetesClient() (*kubernetes.Clientset, error) {
	if minikubeMode {
		config, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
			&clientcmd.ClientConfigLoadingRules{ExplicitPath: filepath.Join(os.Getenv("HOME"), ".kube", "config")},
			&clientcmd.ConfigOverrides{
				CurrentContext: "minikube", // explicitly use minikube for now
			}).ClientConfig()

		if err != nil {
			return nil, err
		}
		return kubernetes.NewForConfig(config)
	}

	vaultClient, err := vaultAPI.NewClient(vaultAPI.DefaultConfig())

	if err != nil {
		return nil, err
	}

	secret, err := vaultClient.Logical().Read(kubeVaultPath)
	if err != nil {
		return nil, err
	}

	configString, ok := secret.Data[environment].(string)
	if !ok {
		return nil, fmt.Errorf("Failed to get kube config string from vault")
	}
	configBytes := []byte(configString)

	tmpfile, err := ioutil.TempFile("", "kube_config")
	if err != nil {
		return nil, err
	}

	defer os.Remove(tmpfile.Name())
	if _, err := tmpfile.Write(configBytes); err != nil {
		return nil, err
	}
	if err := tmpfile.Close(); err != nil {
		return nil, err
	}

	config, err := clientcmd.BuildConfigFromFlags("", tmpfile.Name())
	if err != nil {
		return nil, err
	}

	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	// Ensure the client works
	jobs, err := client.BatchV1().Jobs("default").List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Kubernetes client intialized and loaded %d jobs\n", len(jobs.Items))

	return client, nil
}
