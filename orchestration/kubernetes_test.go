package orchestration

import (
	"bytes"
	"fmt"
	"io"
	"testing"
	"time"

	model "gitlab.com/de1ux/wreckless/generated/api"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type testReadCloser struct {
	buf *bytes.Buffer
}

func (c testReadCloser) Close() error {
	return nil
}

func (c testReadCloser) Read(p []byte) (n int, err error) {
	return c.buf.Read(p)
}

func getReadCloser(text []byte) io.ReadCloser {
	return testReadCloser{
		buf: bytes.NewBuffer(text),
	}
}

func TestGetLogsFailsOnFailureToGetPods(t *testing.T) {
	internalGetJobPods = func(client *kubernetes.Clientset, name string) ([]corev1.Pod, error) {
		return []corev1.Pod{}, fmt.Errorf("")
	}

	o := &v1{nil}

	_, err := o.GetLogs(&model.Execution{})

	if err == nil {
		t.Fatal("Expected error")
	}
}

func TestGetLogsFailsAfterGetPodsRetries(t *testing.T) {
	// Speed up testing
	getLogsInterval = time.Duration(time.Nanosecond * 1)

	internalGetJobPods = func(client *kubernetes.Clientset, name string) ([]corev1.Pod, error) {
		return []corev1.Pod{}, nil
	}

	o := &v1{nil}

	_, err := o.GetLogs(&model.Execution{})

	if err == nil {
		t.Fatal("Expected error")
	}
}

func TestGetLogsFailsAfterGetStreamRetries(t *testing.T) {
	// Speed up testing
	getLogsInterval = time.Duration(time.Nanosecond * 1)
	getJobPodsInterval = time.Duration(time.Nanosecond * 1)

	internalGetJobPods = func(client *kubernetes.Clientset, name string) ([]corev1.Pod, error) {
		return []corev1.Pod{
			corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Name: "",
				},
			},
		}, nil
	}

	internalGetJobPodLogStream = func(client *kubernetes.Clientset, name string) (io.ReadCloser, error) {
		return nil, fmt.Errorf("")
	}

	o := &v1{nil}

	_, err := o.GetLogs(&model.Execution{})

	if err == nil {
		t.Fatal("Expected error")
	}
}

func TestGetLogsSuccess(t *testing.T) {
	// Speed up testing
	getLogsInterval = time.Duration(time.Nanosecond * 1)
	getJobPodsInterval = time.Duration(time.Nanosecond * 1)

	internalGetJobPods = func(client *kubernetes.Clientset, name string) ([]corev1.Pod, error) {
		return []corev1.Pod{
			corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Name: "",
				},
			},
		}, nil
	}

	internalGetJobPodLogStream = func(client *kubernetes.Clientset, name string) (io.ReadCloser, error) {
		return getReadCloser([]byte("test")), nil
	}

	o := &v1{nil}

	c, err := o.GetLogs(&model.Execution{})
	if err != nil {
		t.Fatalf("Unexpected error: %s", err)
	}

	log := ""
	for b := range c {
		log += string(b)
	}

	if log != "test" {
		t.Fatalf("Expected 'test', got %s", log)
	}
}
