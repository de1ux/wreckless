# Wreckless

`wreckless` is a lightweight UI for composing automated jobs into a workflow, powered by Kubernetes Jobs/CronJobs.

### Features

* **Highly available** - Wreckless is designed to run with multiple replicas to ensure availability across regions
* **Auditable** - Details for each job and execution are replicated to a database for long-term auditability


### API

Contracts are available in the [api](api/) folder. See 

* [gRPC](https://grpc.io/) for generating client code in a variety of languages.
* [Makefile's api command](Makefile) for an example of generating code for Go/Typescript.

### Client components

`wreckless` is a web client in [Typescript](https://www.typescriptlang.org/) + [React](https://reactjs.org/). Core components include

* [Dashboard](app/src/Dashboard.tsx) (entrypoint)
* [Definition](app/src/components/job/Definition.tsx) (abstraction for [k8s JobSpec](https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion))

### Server components

[gRPC-go](https://github.com/grpc/grpc-go) provides the API foundation for web clients. Core components include

* [Execution](execution/execution.go) (responsible for persisting requests and orchestrating executions with the modular backend)
* [Orchestration](orchestration/orchestrator.go) (pluggable interface for scheduling tasks)
* [Persistance](persistence/persistence.go) (pluggable interface for persisting task definitions)

### Requirements

* protobuf
* Go 1.9.x
* Typescript 2.6.x
* Minikube with Kubernetes 1.8.x
* npm
* gcloud and a Google Cloud Console project

### Building Wreckless

Install UI dependencies
```bash
$ cd app
$ npm install -g yarn
$ yarn install
```

Ensure protobuf is installed
```bash
$ brew install protobuf
```

Generate protobuffer definitions
```bash
$ make api
```

Authenticate with Google
```bash
$ gcloud auth application-default login
```

Start Minukube and set `kubectl` to the minikube context.
```bash
$ minikube start
$ kubectl config use-context minikube
```

Install Go dependencies
```bash
$ go get -v ./...
```

Start the Go server and Webpack in separate processes
```bash
$ go run main.go
# in another terminal...
$ cd app/
$ npm run start
```

and visit https://0.0.0.0:8080/ in your browser for the UI.

### Troubleshooting
panic: Failed to get Project Keys: Failed to get Project Keys: rpc error: code = FailedPrecondition desc = The Cloud Datastore API is not enabled for the project <project>:

Enable


### Sponsors

* [SoFi](https://www.sofi.com/)
