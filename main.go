package main

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"strings"

	"gitlab.com/de1ux/wreckless/execution"
	model "gitlab.com/de1ux/wreckless/generated/api"
	"gitlab.com/de1ux/wreckless/listeners"
	"gitlab.com/de1ux/wreckless/orchestration"
	"gitlab.com/de1ux/wreckless/persistence"

	"github.com/gin-gonic/gin"
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	port = 8080
	key  = ``
	cert = ``
)

var (
	executor execution.Executor
)

func main() {
	// TODO - break this out into a separate service
	go listeners.Run()

	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		panic(err)
	}

	gsServer := grpc.NewServer()
	model.RegisterWrecklessServiceServer(gsServer, newServer())

	grpcServer := grpcweb.WrapServer(gsServer)

	r := gin.Default()

	// Static asset routes

	if os.Getenv("DEBUG") != "" {
		r.StaticFile("/", "./app/index.html")
		r.GET("/dist/:name", func(c *gin.Context) {
			c.Redirect(http.StatusMovedPermanently, fmt.Sprintf("http://0.0.0.0:9009/%s", c.Param("name")))
		})
		r.GET("/execution/:key", func(c *gin.Context) {
			c.File("./app/index.html")
		})
		r.GET("/project/:key", func(c *gin.Context) {
			c.File("./app/index.html")
		})
	} else {
		r.StaticFile("/", "/app/index.html")
		r.GET("/dist/:name", func(c *gin.Context) {
			c.File(fmt.Sprintf("/app/dist/%s", c.Param("name")))
		})
		r.GET("/execution/:key", func(c *gin.Context) {
			c.File("/app/index.html")
		})
		r.GET("/project/:key", func(c *gin.Context) {
			c.File("/app/index.html")
		})
	}

	if err := (&http.Server{
		Addr:    ":8080",
		Handler: grpcHandlerFunc(grpcServer, r),
	}).Serve(lis); err != nil {
		panic(err)
	}
}

func grpcHandlerFunc(grpcServer http.Handler, otherHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
			grpcServer.ServeHTTP(w, r)
		} else {
			otherHandler.ServeHTTP(w, r)
		}
	})
}

func newServer() model.WrecklessServiceServer {
	o, err := orchestration.NewKubernetes()
	if err != nil {
		panic(err)
	}

	p, err := persistence.NewDatastorePersistor()
	if err != nil {
		panic(err)
	}

	e := execution.NewExecutor(o, p)
	executor = e

	return &wrecklessService{o, p, e}
}

type wrecklessService struct {
	orchestrator orchestration.Orchestrator
	persistor    persistence.Persistor
	executor     execution.Executor
}

func (w *wrecklessService) CreateProject(ctx context.Context, project *model.Project) (*model.Project, error) {
	return w.persistor.CreateProject(project)
}

func (w *wrecklessService) ReadProjectByID(ctx context.Context, key *model.ReadProjectMessage) (*model.Project, error) {
	return w.persistor.ReadProjectByID(key.Id)
}

func (w *wrecklessService) ReadJobsByProjectID(message *model.ReadJobByIdMessage, stream model.WrecklessService_ReadJobsByProjectIDServer) error {
	jobs, err := w.persistor.ReadJobsByProjectID(message.Id)
	if err != nil {
		return err
	}

	for _, j := range jobs {
		if err := stream.Send(j); err != nil {
			return err
		}
	}
	return nil
}

func (w *wrecklessService) GetProjects(_ *model.Empty, stream model.WrecklessService_GetProjectsServer) error {
	projects, err := w.persistor.ReadProjects()
	if err != nil {
		return err
	}

	for _, j := range projects {
		if err := stream.Send(j); err != nil {
			return err
		}
	}
	return nil
}

func (w *wrecklessService) CreateOrUpdateJobDefinition(ctx context.Context, job *model.JobDefinition) (*model.JobDefinition, error) {
	return w.persistor.CreateOrUpdateJobDefinition(job)
}

func (w *wrecklessService) GetJobDefinitions(_ *model.Empty, stream model.WrecklessService_GetJobDefinitionsServer) error {
	jobs, err := w.persistor.ReadJobDefinitions()
	if err != nil {
		return err
	}

	for _, j := range jobs {
		if err := stream.Send(j); err != nil {
			return err
		}
	}
	return nil
}

func (w *wrecklessService) DeleteJobDefinition(ctx context.Context, job *model.JobDefinition) (*model.Empty, error) {
	return &model.Empty{}, w.persistor.DeleteJobDefinition(job)
}

func (w *wrecklessService) CreateExecutionFromDefinitionId(ctx context.Context, execution *model.Execution) (*model.Execution, error) {
	return w.persistor.CreateExecution(execution)
}

func (w *wrecklessService) ModifyExecutionFromExecutionId(ctx context.Context, execution *model.Execution) (*model.Execution, error) {
	if err := w.persistor.UpdateExecution(execution); err != nil {
		return nil, err
	}

	return executor.Schedule(execution)
}

func (w *wrecklessService) GetExecutionFromExecutionId(ctx context.Context, execution *model.GetExecution) (*model.Execution, error) {
	return w.persistor.GetExecutionFromExecutionId(execution.Id)
}

func (w *wrecklessService) GetExecutionLogsFromExecution(execution *model.Execution, stream model.WrecklessService_GetExecutionLogsFromExecutionServer) error {
	c, err := w.orchestrator.GetLogs(execution)
	if err != nil {
		return err
	}

	for i := range c {
		if i == byte('\x0A') {
			stream.Send(&model.ExecutionLog{Log: "\\n"})
		} else {
			stream.Send(&model.ExecutionLog{Log: string(i)})
		}
	}
	return nil
}

func (w *wrecklessService) GetMetrics(_ *model.Empty, stream model.WrecklessService_GetMetricsServer) error {
	metrics, err := w.persistor.GetMetrics()
	if err != nil {
		return err
	}
	for _, m := range metrics {
		if err := stream.Send(m); err != nil {
			return err
		}
	}
	return nil
}
