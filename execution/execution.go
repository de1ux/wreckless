package execution

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	model "gitlab.com/de1ux/wreckless/generated/api"
	"gitlab.com/de1ux/wreckless/orchestration"
	"gitlab.com/de1ux/wreckless/persistence"

	"github.com/google/uuid"
)

func hashJobName(name string) string {
	hyphened := strings.Replace(name, " ", "-", -1)
	lowercased := strings.ToLower(hyphened)
	return fmt.Sprintf("%s-%s", lowercased, strings.Split(uuid.New().String(), "-")[0])
}

var kubernetesJobManifestTemplate = `
apiVersion: batch/v1
kind: Job
metadata:
  name: {{.Name}}
spec:
  completions: 1
  template:
    spec:
      containers:
      - name: {{.Name}}
        image: {{.Image}}
        imagePullPolicy: Never
        env:{{ range $k, $o := .Options }}
        - name: {{ $o.Name }}
          value: "{{ $o.Value }}"{{ end }}
      restartPolicy: Never`

var kubernetesCronJobManifestTemplate = `
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: {{.Name}}
spec:
  schedule: "{{.Schedule}}"
  jobTemplate:
    spec:
      template:
        spec:
          restartPolicy: Never
          containers:
          - name: {{.Name}}
            image: {{.Image}}
            imagePullPolicy: Never
            env:{{ range $k, $o := .Options }}
            - name: {{ $o.Name }}
              value: "{{ $o.Value }}"{{ end }}`

type Executor struct {
	orchestrator orchestration.Orchestrator
	persistor    persistence.Persistor
}

func NewExecutor(o orchestration.Orchestrator, p persistence.Persistor) Executor {
	return Executor{o, p}
}

func (e Executor) Schedule(execution *model.Execution) (*model.Execution, error) {
	job, err := e.persistor.ReadJobDefinitionById(execution.JobDefinitionKey)
	if err != nil {
		return execution, fmt.Errorf("Failed to read job definition by id: %s", err)
	}
	execution.Hash = hashJobName(job.Name)

	if err := e.persistor.UpdateExecution(execution); err != nil {
		return execution, fmt.Errorf("Failed to update job execution hash: %s", err)
	}

	jobManifest, err := e.toKubernetesJobManifest(job, execution)
	if err != nil {
		return nil, err
	}

	if execution.ScheduleType == model.Execution_NOW {
		return execution, e.orchestrator.RunJobToCompletion(execution.Hash, jobManifest)
	} else {
		return execution, e.orchestrator.ScheduleJob(job.Name, jobManifest)
	}

	return execution, nil
}

func (e Executor) toKubernetesJobManifest(job *model.JobDefinition, execution *model.Execution) (string, error) {
	type option struct {
		Name  string
		Value string
	}

	options := make([]*option, len(execution.Options))
	for i, o := range execution.Options {
		jobOption, err := e.persistor.ReadJobOptionById(o.JobDefinitionOptionKey)
		if err != nil {
			return "", fmt.Errorf("Failed to read job options by id: %s", err)
		}
		options[i] = &option{jobOption.Name, o.Value}
	}

	d := &struct {
		Name     string
		Image    string
		Options  []*option
		Schedule string
	}{execution.Hash, job.Image, options, execution.ScheduleValue}

	tmpl := kubernetesJobManifestTemplate
	if execution.ScheduleType == model.Execution_LATER {
		tmpl = kubernetesCronJobManifestTemplate
	}

	buf := new(bytes.Buffer)
	t := template.Must(template.New("kubernetesManifest").Parse(tmpl))
	if err := t.Execute(buf, d); err != nil {
		return "", fmt.Errorf("Failed to parse template: %s", err)
	}

	println(buf.String())
	return buf.String(), nil
}
